package com.founder.service;

import com.founder.core.domain.MchNotify;

public interface IMchNotifyService {

    /**
     * 查询商户通知
     * @param orderId
     * @return
     */
    MchNotify selectMchNotify(String orderId);

    /**
     * 更新通知次数
     * @param orderId
     * @param cnt
     * @return
     */
    int updateNotify4Count(String orderId, Integer cnt);

    /**
     * 保存
     * @param mchNotify
     * @return
     */
    int saveMchNotify(MchNotify mchNotify);
}
